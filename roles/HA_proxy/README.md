
# ndaal_ansible_role_HA_proxy

Ansible role for HA proxy.

## Description

This role sets up an HA proxy server to work as a Load balancer for servers.


## Pre-requisite

- [ ] Python 3

## Role variables

In `/defaults/main.yml`, the following role variable need to be configured. For `ha_proxy services`, define which service need to be configured.
```yaml
---
nodes:
  - xx.xx.xx.xx
  - oo.oo.oo.oo

username: <username defined>
password: <password defined>

haproxy_services:
  - name: <Define name>
    bind: <Define port>
    servers: "{{ nodes }}"
    server_opts:
      - check
      - check-ssl
      - verify
      - none
    description: <"Define Description">
    mode: <Define mode>
    frontend_config:
      - "option tcplog"
      - "timeout client 30000"
      - "log global"
    backend_config:
      - "option httpchk GET /v1/sys/health"
      - "timeout check 5000"
      - "timeout server 30000"
      - "timeout connect 5000"
```

## Example Playbook

Including example of how to run HA proxy.

```yaml
---
- hosts: all
  become: true
  become_method: sudo
  roles:
    - HA_proxy
```
- Run roles for `Vault`  first (Consul, Vault_install, Vault_init, Vault_unseal) if one want to run HA proxy as load balancer.

## Testing

Molecule testing for HA_proxy coming soon.

## Roadmap

Molecule testing of HA proxy to be soon implementated.

## Reading
For further reading and improvements, i.e to add certifcates: [Secure HA proxy](https://computingforgeeks.com/understanding-secure-haproxy-in-linux/)

## Contributing
Any Contribution to improve the role is welcome. Please see Roadmap for features and improvements.

## Authors and acknowledgment

[Mamoona Aslam](mamoona.aslam@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne

## License
[The GNU General Public License, Version 3](LICENSE)
