"""Test whether the haproxy installation was successful."""

import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_haproxy_installed(host):
    """Test if haproxy installation was successful."""
    haproxy_package_name = _get_haproxy_package_name(
        host.system_info.distribution
    )
    haproxy_package = host.package(haproxy_package_name)
    assert haproxy_package.is_installed


def test_haproxy_service_started_enabled(host):
    """Test if haproxy is started successfully."""
    haproxy_service_name = _get_haproxy_package_name(
        host.system_info.distribution
    )
    haproxy_service = host.service(haproxy_service_name)
    assert haproxy_service.is_running
    assert haproxy_service.is_enabled


def _get_haproxy_package_name(host_distro):
    return {"debian": "haproxy"}.get(host_distro, "haproxy")
