"""Test whether the vault installation was successful."""

import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_vault_installed(host):
    """Test if vault installation was successful."""
    vault_package_name = _get_vault_package_name(
        host.system_info.distribution
    )
    vault_package = host.package(vault_package_name)
    assert vault_package.is_installed


def test_vault_service_started_enabled(host):
    """Test if vault is started successfully."""
    vault_service_name = _get_vault_package_name(
        host.system_info.distribution
    )
    vault_service = host.service(vault_service_name)
    assert vault_service.is_running
    assert vault_service.is_enabled


def _get_vault_package_name(host_distro):
    return {"debian": "vault"}.get(host_distro, "vault")
