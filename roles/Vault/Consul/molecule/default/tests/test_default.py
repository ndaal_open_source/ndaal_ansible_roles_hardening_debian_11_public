"""Test whether the consul installation was successful."""

import os

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ["MOLECULE_INVENTORY_FILE"]
).get_hosts("all")


def test_consul_installed(host):
    """Test if consul installation was successful."""
    consul_package_name = _get_consul_package_name(
        host.system_info.distribution
    )
    consul_package = host.package(consul_package_name)
    assert consul_package.is_installed


def test_consul_service_started_enabled(host):
    """Test if consul is started successfully."""
    consul_service_name = _get_consul_package_name(
        host.system_info.distribution
    )
    consul_service = host.service(consul_service_name)
    assert consul_service.is_running
    assert consul_service.is_enabled


def _get_consul_package_name(host_distro):
    return {"debian": "consul"}.get(host_distro, "consul")
