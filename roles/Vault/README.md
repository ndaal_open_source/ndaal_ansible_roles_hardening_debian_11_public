
# ndaal_ansible_role_hashicorp_vault

Ansible role for Hashicorp Vault and Consul along with HA proxy.

## Description

This role generates self-signed certificate and pass as a runtime parameter to Influxd command. Hence influxdb2 endpoint is accessible via HTTPS (TLS enabled).


## Pre-requisite

- [ ] Python 3

Run following command in the shell for package installation.

``
pip3 install -r requirements.txt
``

## Dependencies

A Role named common is included and should work as a dependency For Debian 11.

## Role variables

In Consul role `/defaults/main.yml`, the IP addresses of the machines which form the vault cluster are specified.
```yaml
---
vault_node_1: <IP address of Node 1>
vault_node_2: <IP address of Node 2>
vault_node_3: <IP address of Node 3>
vault_node_4: <IP address of Node 4>
vault_node_5: <IP address of Node 5>
```
These variables can also be defined in `vars` playbook as described in example playbooks.

## Example Playbook

Including examples of how to incorporate Hashicorp vault cluster.

- Install Consul before Vault on all nodes.

```yaml
---
- hosts: all
  become: true
  become_method: sudo
  roles:
    - cosul
    - vault_install
    vars:
      vault_node_1: <IP address of Node 1>
      vault_node_2: <IP address of Node 2>
      vault_node_3: <IP address of Node 3>
      vault_node_4: <IP address of Node 4>
      vault_node_5: <IP address of Node 5>
```
- For initilization, Only run the role `Vault_init` on one node.


```yaml
---
- hosts: all
  roles:
    - vault_init
```
- Then run `Vault_unseal` role on all nodes. This role can be run at times when nodes are restarted and the vaults need to be unsealed.


```yaml
---
- hosts: all
  become: true
  become_method: sudo
  roles:
    - vault_unseal
```

## Testing

Molecule testing for vaults coming soon.

## Roadmap

Molecule testing of hashicorp vault, consul and HA proxy to be soon implementated.

## Contributing
Any Contribution to improve the role is welcome. Please see Roadmap for features and improvements.

## Authors and acknowledgment

[Mamoona Aslam](mamoona.aslam@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne

## License
[The GNU General Public License, Version 3](LICENSE)
