![](/images/logo.png)
# ndaal_ansible_role_common

Ansible role for Common contains installation of required apt and pip packages.

## Description

This role generates installation of some common apt and pip packages which are required for different roles in our Ndaal Repo along with cache update and updgrade.


## Role variables

These variables should be setup to use this role. TheY are declared in default/main.yml

| Variable  | Default | Description |
| ---  | --- | --- |
| update_cache_value | true | Value to be set true in case of apt update
| upgrade_apt | no | If **yes** or **safe**, performs an aptitude safe-upgrade. If **full**, performs an aptitude full-upgrade. If **dist**, performs an apt-get dist-upgrade. *Note*: This does not upgrade a specific package, use state=latest for that. *Note*: Since 2.4, apt-get is used as a fall-back if aptitude is not present.
| apt_packages | --- | List of common apt packages required for installation
| pip_packages | --- | List of common pip packages required for installation


## Pre-requisite

- [ ] Python 3

## Example Playbook

Including an example of how to use your role.

```
---
- hosts: all
  roles:
    - common
```

## Contributing
Any Contribution to improve the role is welcome.

## Authors and acknowledgment

[Mamoona Aslam](mamoona.aslam@ndaal.eu) / ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne

## License
[The GNU General Public License, Version 3](LICENSE)
