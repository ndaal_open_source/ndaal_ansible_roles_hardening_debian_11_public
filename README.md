![](/images/logo.png)
## ndaal Ansible roles for Hardening and Secret management

## Description
This Repository contains Ansible roles for Secret Management (Hashicorp Vault & Console), HA proxy and Hardening for Debian and Ubuntu LTS versions (20.04, 22.04)

## Roles

Initially `Common` role is run, then `Vault`, afterwards `HA_proxy` and Later `Hardening` role.
```sh
├── Common
├── HA_proxy
├── Hardening
├── Vault
    ├── Consul
    ├── Vault_install
    ├── Vault_init
    ├── Vault_unseal
```


## Contributing

Any Contribution to improve the role is welcome. Please see Roadmap for features and improvements.

## Author Information

[Mamoona Aslam](mamoona.aslam@ndaal.eu) /
[Pierre Gronau](pierre.gronau@ndaal.eu) /
 ndaal Gesellschaft für Sicherheit in der Informationstechnik mbH & Co KG in Cologne